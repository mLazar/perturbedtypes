////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *     VoroTop: Voronoi Cell Topology     *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *             (Version 0.4)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *      University of Pennsylvania        *   ////
////   *           December 5, 2014             *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: vorotop.cc

#include <random>
#include <vector>
#include <cstring>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"
#include <chrono>



int main(int argc, char *argv[])
{
    number_of_particles = 9;
    all_wvectors.resize (number_of_particles);          // MEMORY FOR WVECTORS
    
        std::default_random_engine generator;
        generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
        perturbation_size = 0.001;
        std::normal_distribution<double> distribution(0., perturbation_size);
    
    Filter filter;
    
    //Create my data
    double initial_emplacement [11][3];
    
    initial_emplacement[0][0]= 0; initial_emplacement[0][1]= 0; initial_emplacement[0][2]= 0;   // ORIGIN, MAIN GUY

    initial_emplacement[1][0]=-1; initial_emplacement[1][1]= 0; initial_emplacement[1][2]= 0;   // SIX FULL FACE NEIGHBORS
    initial_emplacement[2][0]= 1; initial_emplacement[2][1]= 0; initial_emplacement[2][2]= 0;
    initial_emplacement[3][0]= 0; initial_emplacement[3][1]=-1; initial_emplacement[3][2]= 0;
    initial_emplacement[4][0]= 0; initial_emplacement[4][1]= 1; initial_emplacement[4][2]= 0;
    initial_emplacement[5][0]= 0; initial_emplacement[5][1]= 0; initial_emplacement[5][2]=-1;
    initial_emplacement[6][0]= 0; initial_emplacement[6][1]= 0; initial_emplacement[6][2]= 1;
    
    initial_emplacement[7][0]= 0; initial_emplacement[7][1]= 1; initial_emplacement[7][2]= 1;
    initial_emplacement[8][0]= 1; initial_emplacement[8][1]= 1; initial_emplacement[8][2]= 0;
    initial_emplacement[9][0]= 1; initial_emplacement[9][1]= 0; initial_emplacement[9][2]= 1;
    initial_emplacement[10][0]=1; initial_emplacement[10][1]= 1; initial_emplacement[10][2]= 1;


    // UNTIL THIS POINT, WE HAVE COMPUTED VECTORS POINTING FROM EVERY VERTEX i TO
    // EVERY OTHER VERTEX j.  WE WILL ITERATE OVER ALL POSSIBLE DIRECTIONS AND COMPUTE
    // THE VORONOI CELLS AFTER THESE PERTURBATIONS
    
    for(int c=0; c<11; c++)
        for(int d=0; d<3; d++)
            initial_emplacement[c][d]+=5.;
    
    
    for(int iteration=0; iteration<10000000; iteration++)
    {
        // BUILDS CONTAINER AND ADDS PARTICLES
        voro::particle_order vo;
        voro::container_periodic con (10.,0.,10.,0.,0.,10.,1,1,1,1);
        
        double x, y, z;
        for (int counter = 0; counter <7; counter++)
        {
            x = initial_emplacement[counter][0];
            y = initial_emplacement[counter][1];
            z = initial_emplacement[counter][2];
            con.put(vo,counter, x,y,z);
            //std::cout << counter << '\t' << x << '\t' << y << '\t' << z << '\n';
        }

        for (int counter = 7; counter <11; counter++)
        {
            x = initial_emplacement[counter][0]+distribution(generator);
            y = initial_emplacement[counter][1]+distribution(generator);
            z = initial_emplacement[counter][2]+distribution(generator);
            con.put(vo,counter, x,y,z);
            //std::cout << counter << '\t' << x << '\t' << y << '\t' << z << '\n';
        }

//        con.put(vo,7,7.+distribution(generator),7.+distribution(generator),5.+distribution(generator));
        
        
        voro::c_loop_order_periodic vlo(con,vo);
        voro::voronoicell vcell;
           
        // WE COMPUTE THE VORONOI TOPOLOGY OF THE CENTRAL PARTICLE
        if(vlo.start())
        {
            if(con.compute_cell(vcell,vlo))
               all_wvectors[0] = calc_wvector(vcell,0);
        }
        
        // ADDS TOPOLOGY OF FIRST VORONOI CELL, THE ONE IN THE MIDDLE
        filter.increment_or_add(all_wvectors[0],1);
    }
    
    std::stringstream my_filename;
    my_filename << "testing.wvectors";
    filename_output = my_filename.str();
    filter.print_distribution(filename_output);

    std::cout << "We're here!\n";
    std::cout << "We have " << filter.get_size() << " types\n";
//    std::string  deb="testing_wvectors";
//    filename_output = deb;//"testing.wvectors";
//    filter.print_distribution(filename_output);
    
    return 0;
}









