////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *     VoroTop: Voronoi Cell Topology     *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *             (Version 0.4)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *      University of Pennsylvania        *   ////
////   *           December 5, 2014             *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: vorotop.cc

#include <random>
#include <vector>
#include <cstring>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"




int main(int argc, char *argv[])
{
    number_of_particles = 9;
    all_wvectors.resize (number_of_particles);          // MEMORY FOR WVECTORS
    
        std::default_random_engine generator;
        perturbation_size = 0.0001;
        std::normal_distribution<double> distribution(0., perturbation_size);
    
    Filter filter;
    
    //Create my data
    double initial_emplacement [9][3];
    
    initial_emplacement[0][0]=1; initial_emplacement[0][1]=2; initial_emplacement[0][2]=2;
    initial_emplacement[1][0]=1; initial_emplacement[1][1]=1; initial_emplacement[1][2]=1;
    initial_emplacement[2][0]=2; initial_emplacement[2][1]=1; initial_emplacement[2][2]=1;
    initial_emplacement[3][0]=2; initial_emplacement[3][1]=2; initial_emplacement[3][2]=1;
    initial_emplacement[4][0]=1; initial_emplacement[4][1]=2; initial_emplacement[4][2]=1;
    initial_emplacement[5][0]=1; initial_emplacement[5][1]=1; initial_emplacement[5][2]=2;
    initial_emplacement[6][0]=2; initial_emplacement[6][1]=1; initial_emplacement[6][2]=2;
    initial_emplacement[7][0]=2; initial_emplacement[7][1]=2; initial_emplacement[7][2]=2;
    initial_emplacement[8][0]=1.5; initial_emplacement[8][1]=1.5; initial_emplacement[8][2]=1.5;
    
    /*con.put(vo, 0, 1.5, 1.5, 1.5);
     con.put(vo, 1, 1, 1, 1);
     con.put(vo, 2, 2, 1, 1);
     con.put(vo, 3, 2, 2, 1);
     con.put(vo, 4, 1, 2, 1);
     con.put(vo, 5, 1, 1, 2);
     con.put(vo, 6, 2, 1, 2);
     con.put(vo, 7, 2, 2, 2);
     con.put(vo, 8, 1, 2, 2);
     */
    
    
    double my_amplitude = 0.01;
    
    // Matrix direction - to get the direction of motion of each point
    double matrix_direction[9][17][3], norm;
    
    // WE BEGIN BY CALCULATING ALL VECTORS POINTING FROM POINTS TO OTHER POINTS
    // FOR EACH OF THE NINE PARTICLES
    for (int i = 0; i < 9; i++)
    {
        // FOR A PARTICLE i, CALCULATE THE VECTOR POINTING FROM PARTICLE i TO ITS 8 NEIGHBORS, INDEXED BY j
        // VECTORS WILL BE NORMALIZED, ALL HAVING LENGTH my_amplitude
        for (int j = 0; j < 9; j++)
        {
            matrix_direction[i][j][0] = initial_emplacement[j][0] - initial_emplacement[i][0];
            matrix_direction[i][j][1] = initial_emplacement[j][1] - initial_emplacement[i][1];
            matrix_direction[i][j][2] = initial_emplacement[j][2] - initial_emplacement[i][2];
            norm = sqrt(matrix_direction[i][j][1] * matrix_direction[i][j][1]
                        + matrix_direction[i][j][2] * matrix_direction[i][j][2]
                        + matrix_direction[i][j][0] * matrix_direction[i][j][0]);
            
            // THIS MAKES SURE THAT ALL VECTORS HAVE LENGTH my_amplitude
            if (norm > 0.0001) {
                matrix_direction[i][j][0] = my_amplitude * matrix_direction[i][j][0] / norm;
                matrix_direction[i][j][1] = my_amplitude * matrix_direction[i][j][1] / norm;
                matrix_direction[i][j][2] = my_amplitude * matrix_direction[i][j][2] / norm;
            }
        }
        
        // FOR EACH VECTOR POINTING FROM i TO j, ANOTHER VECTOR POINTS FROM j TO i
        int j_index=0;
        for (int j = 0; j < 9; j++) if (i!=j)
        {
            matrix_direction[i][j_index + 9][0] = -matrix_direction[i][j][0];
            matrix_direction[i][j_index + 9][1] = -matrix_direction[i][j][1];
            matrix_direction[i][j_index + 9][2] = -matrix_direction[i][j][2];
            j_index = j_index+1;
        }
    }
    

    // UNTIL THIS POINT, WE HAVE COMPUTED VECTORS POINTING FROM EVERY VERTEX i TO
    // EVERY OTHER VERTEX j.  WE WILL ITERATE OVER ALL POSSIBLE DIRECTIONS AND COMPUTE
    // THE VORONOI CELLS AFTER THESE PERTURBATIONS
    
    // Check all possible combinations
    int direction_index[9], my_line, flag;
    unsigned long int loop_count = 0;
    
    //initial condition
    for (int i = 0; i < 9; i++) {
        direction_index[i] = 0;
    }
    
    my_line = number_of_particles-1;
    flag = 0;
    int direction_amount = 2*number_of_particles-1;
    
    while (flag < number_of_particles-1)
    {
        loop_count = loop_count + 1;
        if (loop_count%1000000==0) std::cout << loop_count << '\n';
        
        // THIS SECTION COMPUTES THE DISPLACEMENT VECTORS FOR THIS ITERATION
        if (direction_index[my_line] < direction_amount-1) {
            direction_index[my_line] = direction_index[my_line] + 1;
        }
        else if (direction_index[my_line] == direction_amount-1 && my_line > 0)
        {
            while (direction_index[my_line] == direction_amount-1 && my_line > 0)
                my_line = my_line - 1;

            flag = 0;
            
            for (int i = 0; i < number_of_particles; i++) {
                if (direction_index[i] == direction_amount-1) {
                    flag = flag + 1;
                }
            }

            /*
            if (flag == number_of_particles-1)
            {
                std::cout << "here \n";
                std::cout << "flag is " << flag << "\n";
                std::cout << " loop_count = " << loop_count << "\n";
                break;
            }*/
            
            direction_index[my_line] = direction_index[my_line] + 1;
            
            for (int i = (number_of_particles-1); i > my_line; i--)
                direction_index[i] = 0;

            my_line = number_of_particles-1;
        }
        
        
        
        // BUILDS CONTAINER AND ADDS PARTICLES
        voro::particle_order vo;
        voro::container_periodic con (3.,0.,3.,0.,0.,3.,1,1,1,1);
        
        double x, y, z;
        for (int counter = 8; counter >= 0; counter--)
        {
            x = initial_emplacement[counter][0] + matrix_direction[counter][direction_index[counter]][0];
            y = initial_emplacement[counter][1] + matrix_direction[counter][direction_index[counter]][1];
            z = initial_emplacement[counter][2] + matrix_direction[counter][direction_index[counter]][2];
            con.put(vo,8-counter, x,y,z);
        }

        voro::c_loop_order_periodic vlo(con,vo);
        voro::voronoicell vcell;
           
        // WE COMPUTE THE VORONOI TOPOLOGY OF THE CENTRAL PARTICLE
        if(vlo.start())
        {
            if(con.compute_cell(vcell,vlo))
               all_wvectors[0] = calc_wvector(vcell,0);
        }
        
        // ADDS TOPOLOGY OF FIRST VORONOI CELL, THE ONE IN THE MIDDLE
        filter.increment_or_add(all_wvectors[0],1);
                
        // WE DO ROUGHLY 1 MILLION ITERATIONS PER MINUTE
        if (loop_count%1000000==0){
            std::stringstream my_filename;
            my_filename << "testing.wvectors"<<loop_count;
            filename_output = my_filename.str();
            filter.print_distribution(filename_output);
            exit(0);
        }
    } //End of check all possible combination

    std::cout << "We're here!\n";
//    std::string  deb="testing_wvectors";
//    filename_output = deb;//"testing.wvectors";
//    filter.print_distribution(filename_output);
    
    return 0;
}









