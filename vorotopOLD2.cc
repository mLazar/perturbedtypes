////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *     VoroTop: Voronoi Cell Topology     *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *             (Version 0.4)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *      University of Pennsylvania        *   ////
////   *           December 5, 2014             *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: vorotop.cc

#include <random>
#include <vector>
#include <cstring>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"
#include <chrono>



int main(int argc, char *argv[])
{
    number_of_particles = 9;
    all_wvectors.resize (number_of_particles);          // MEMORY FOR WVECTORS
    
        std::default_random_engine generator;
        generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
        perturbation_size = 0.01;
        std::normal_distribution<double> distribution(0., perturbation_size);
    
    Filter filter;
    
    //Create my data
    double initial_emplacement [9][3];
    
    initial_emplacement[0][0]= 5; initial_emplacement[0][1]= 5; initial_emplacement[0][2]= 5;   // ORIGIN, MAIN GUY

    initial_emplacement[1][0]= 3; initial_emplacement[1][1]= 5; initial_emplacement[1][2]= 5;   // SIX FULL FACE NEIGHBORS
    initial_emplacement[2][0]= 7; initial_emplacement[2][1]= 5; initial_emplacement[2][2]= 5;
    initial_emplacement[3][0]= 5; initial_emplacement[3][1]= 3; initial_emplacement[3][2]= 5;
    initial_emplacement[4][0]= 5; initial_emplacement[4][1]= 7; initial_emplacement[4][2]= 5;
    initial_emplacement[5][0]= 5; initial_emplacement[5][1]= 5; initial_emplacement[5][2]= 3;
    initial_emplacement[6][0]= 5; initial_emplacement[6][1]= 5; initial_emplacement[6][2]= 7;
    


    // UNTIL THIS POINT, WE HAVE COMPUTED VECTORS POINTING FROM EVERY VERTEX i TO
    // EVERY OTHER VERTEX j.  WE WILL ITERATE OVER ALL POSSIBLE DIRECTIONS AND COMPUTE
    // THE VORONOI CELLS AFTER THESE PERTURBATIONS
    
    
    for(int iteration=0; iteration<100000; iteration++)
    {
        // BUILDS CONTAINER AND ADDS PARTICLES
        voro::particle_order vo;
        voro::container_periodic con (10.,0.,10.,0.,0.,10.,1,1,1,1);
        
        double x, y, z;
        for (int counter = 0; counter <7; counter++)
        {
            x = initial_emplacement[counter][0];
            y = initial_emplacement[counter][1];
            z = initial_emplacement[counter][2];
            con.put(vo,counter, x,y,z);
        }
//        std::cout << distribution(generator) << '\n';
        con.put(vo,7,7.+distribution(generator),7.+distribution(generator),5.+distribution(generator));
        
        
        voro::c_loop_order_periodic vlo(con,vo);
        voro::voronoicell vcell;
           
        // WE COMPUTE THE VORONOI TOPOLOGY OF THE CENTRAL PARTICLE
        if(vlo.start())
        {
            if(con.compute_cell(vcell,vlo))
               all_wvectors[0] = calc_wvector(vcell,0);
        }
        
        // ADDS TOPOLOGY OF FIRST VORONOI CELL, THE ONE IN THE MIDDLE
        filter.increment_or_add(all_wvectors[0],1);
    }
    
        // WE DO ROUGHLY 1 MILLION ITERATIONS PER MINUTE
//        if (loop_count%1000000==0){
            std::stringstream my_filename;
            my_filename << "testing.wvectors";
            filename_output = my_filename.str();
            filter.print_distribution(filename_output);
//            exit(0);
//        }

    std::cout << "We're here!\n";
//    std::string  deb="testing_wvectors";
//    filename_output = deb;//"testing.wvectors";
//    filter.print_distribution(filename_output);
    
    return 0;
}









