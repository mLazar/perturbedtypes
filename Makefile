########################################################
####                                                ####
####   ******************************************   ####
####   *                                        *   ####
####   *     VoroTop: Voronoi Cell Topology     *   ####
####   *   Visualization and Analysis Toolkit   *   ####
####   *             (Version 0.3)              *   ####
####   *                                        *   ####
####   *           Emanuel A. Lazar             *   ####
####   *      University of Pennsylvania        *   ####
####   *           December 5, 2014             *   ####
####   *                                        *   ####
####   ******************************************   ####
####                                                ####
########################################################

####   File: Makefile

# C++ compiler
CXX      = g++

# Flags for the C++ compiler
LFLAGS   = -lvoro++ 
CXXFLAGS = -Wall -O3 -c -std=c++11 


vorotop : vorotop.o wvectors.o variables.o filters.o import.o functions.o analysis.o output.o 
	$(CXX) import.o vorotop.o wvectors.o variables.o filters.o functions.o analysis.o output.o $(LFLAGS) -o PerturbedTypes 

vorotop.o : vorotop.cc filters.hh variables.hh functions.hh
	$(CXX) $(CXXFLAGS) $(LIBS) vorotop.cc

wvectors.o : wvectors.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) $(LIBS) wvectors.cc

variables.o : variables.cc
	$(CXX) $(CXXFLAGS) $(LIBS) variables.cc

filters.o : filters.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) $(LIBS) filters.cc

import.o : import.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) $(LIBS) import.cc

functions.o : functions.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) $(LIBS) functions.cc

analysis.o : analysis.cc filters.hh variables.hh
	$(CXX) $(CXXFLAGS) $(LIBS) analysis.cc

output.o : output.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) $(LIBS) output.cc

zip :
	zip -r package.zip *.cc *.hh LICENSE README Makefile  

clean :
	rm -f *.o PerturbedTypes 


