////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *     VoroTop: Voronoi Cell Topology     *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *             (Version 0.4)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *      University of Pennsylvania        *   ////
////   *           December 5, 2014             *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: vorotop.cc

#include <algorithm>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <vector>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"



struct generalized_motion {
    int    partA;
    int    partB;
    double dx, dy, dz;
};


// ALGORITHM READS IN DATA, AND COMPUTES THE VORONOI TYPES ASSOCIATED
// WITH ALL SMALL PERTURBATIONS OF THE NEIGHBORS, STABLE AND UNSTABLE.

int main(int argc, char *argv[])
{
    ////////////////////////////////////////////////////
    ////
    ////   PARSE ALL COMMAND-LINE OPTIONS.  DETERMINE
    ////   FILE NAMES AND DIRECTORIES TO USE, AND
    ////   WHICH ANALYSES TO PERFORM.
    ////
    ////////////////////////////////////////////////////
    
    parse_arguments(argc, argv);
    
    
    ////////////////////////////////////////////////////
    ////
    ////   OPEN INPUT FILE
    ////
    ////////////////////////////////////////////////////
    
    data_file.open(name_of_data_file, std::ifstream::in);
    if(!data_file.is_open())
    {
        help_message();
        std::cerr << "Unable to open input file " << name_of_data_file << "\n\n";
        exit(-1);
    }
    
    
    ////////////////////////////////////////////////////
    ////
    ////   PARSE DATA FILE TO DETERMINE FILE TYPE
    ////
    ////////////////////////////////////////////////////
    
    file_type = parse_header(data_file);
    
    
    ////////////////////////////////////////////////////
    ////
    ////   CREATE SPACE FOR DATA
    ////
    ////////////////////////////////////////////////////
    
    all_wvectors.resize       (number_of_particles);          // MEMORY FOR WVECTORS
    vt_structure_types.resize (number_of_particles);    // MEMORY FOR STRUCTURE TYPES
    
    // WE NEED NEIGHBOR INFORMATION
    neighbors_list.resize  (number_of_particles);
    neighbors_list_c.resize(number_of_particles);
    
    ////////////////////////////////////////////////////
    ////
    ////   VARIABLES AND HELP MESSAGES IF REQUESTED
    ////
    ////////////////////////////////////////////////////
    
    if(i_switch==0)
    {
        help_message();
        exit(-1);
    }
    
    
    ////////////////////////////////////////////////////
    ////
    ////   CREATE VORO++ CONTAINER WITH NECESSARY DIMENSIONS;
    ////   THEN ADD PARTICLES TO THE CONTAINER.  WE NEED THIS
    ////   STEP TO COMPUTE ALL INITIAL DATA ABOUT PARTICLES
    ////   AND ITS NEIGHBORS.
    ////
    ////////////////////////////////////////////////////
    
    voro::particle_order vo;
    voro::container_periodic con (supercell_edges[0][0],0.,supercell_edges[1][1],0.,0.,supercell_edges[2][2],1,1,1,1);

    if     (file_type==1) import_dump_file   (data_file, con, vo);
    else
    {
        std::cout << "File format not recognized\n";
        exit(-1);
    }
    
    c_switch=1;                     // WE NEED NEIGHBOR DATA
    calc_all_wvectors(con,vo,1);    // COMPUTES VORONOI CELLS AND NEIGHBOR DATA
    
    
    // THE FCC CRYSTAL HAS 12 NEAREST NEIGHBORS, AND HAS 6 MORE PARTICLES
    // THAT *CAN* BECOME NEIGHBORS, BUT NOT NECESSARILY.  HOW DOES A
    // TRIANGULAR FACE HAPPEN?
    
    // COMPUTES DISTANCE OF EACH PARTICLE FROM CENTER (USING PERIODIC
    // BOUNDARIES). THIS IS IMPORTANT FOR DETERMINING WHETHER PARTICLES
    // ARE FULL/FRACTIONAL/NON-NEIGHBORS.
    
    std::vector<std::pair<double, int> > particle_distances_from_center;
    for(int c=0; c<coordinates[0].size(); c++)
    {
        // FOR EACH PARTICLE IN THIS LIST, WE'LL COMPUTE
        // DISTANCE FROM CENTRAL PARTICLE, GIVEN PERIODIC
        // BOUNDARIES.  WE MIGHT AT SOME POINT SKIP PRIMARY
        // NIEGHBORS, WHICH I GUESS WE ALREADY HAVE ABOVE
        // FOR NOW INCLUDING ALL
        double dx = coordinates[0][c] - coordinates[0][0];
        double dy = coordinates[1][c] - coordinates[1][0];
        double dz = coordinates[2][c] - coordinates[2][0];
        
        if(dx > supercell_edges[0][0]/2.) dx -= supercell_edges[0][0];
        if(dx <-supercell_edges[0][0]/2.) dx += supercell_edges[0][0];
        if(dy > supercell_edges[1][1]/2.) dy -= supercell_edges[1][1];
        if(dy <-supercell_edges[1][1]/2.) dy += supercell_edges[1][1];
        if(dz > supercell_edges[2][2]/2.) dz -= supercell_edges[2][2];
        if(dz <-supercell_edges[2][2]/2.) dz += supercell_edges[2][2];
        
        double distance = sqrt(dx*dx+dy*dy+dz*dz);
        particle_distances_from_center.push_back(std::make_pair(distance, c));
    }
    
    /*
     // THIS IS NOT USUALLY NEEDED.  WE USE IT FOR A SPECIAL CASE OF A
     // NON-LATTICE, WHEN WE WANT TO ADD DISTANCES FOR LATER USE
     for(int c=0; c<coordinates[0].size(); c++)
     {
     // FOR EACH PARTICLE IN THIS LIST, WE'LL COMPUTE
     // DISTANCE FROM CENTRAL PARTICLE, GIVEN PERIODIC
     // BOUNDARIES.  WE MIGHT AT SOME POINT SKIP PRIMARY
     // NIEGHBORS, WHICH I GUESS WE ALREADY HAVE ABOVE
     // FOR NOW INCLUDING ALL
     double dx = coordinates[0][c] - coordinates[0][1];
     double dy = coordinates[1][c] - coordinates[1][1];
     double dz = coordinates[2][c] - coordinates[2][1];
     
     if(dx > supercell_edges[0][0]/2.) dx -= supercell_edges[0][0];
     if(dx <-supercell_edges[0][0]/2.) dx += supercell_edges[0][0];
     if(dy > supercell_edges[1][1]/2.) dy -= supercell_edges[1][1];
     if(dy <-supercell_edges[1][1]/2.) dy += supercell_edges[1][1];
     if(dz > supercell_edges[2][2]/2.) dz -= supercell_edges[2][2];
     if(dz <-supercell_edges[2][2]/2.) dz += supercell_edges[2][2];
     
     double distance = sqrt(dx*dx+dy*dy+dz*dz);
     particle_distances_from_center.push_back(std::make_pair(distance, c));
     }
     */
    
    
    // CREATES LIST OF ALL DISTANCES IN SYSTEM
    std::vector <double> all_distances_from_center;
    for(int c=0; c<particle_distances_from_center.size(); c++)
        all_distances_from_center.push_back(particle_distances_from_center[c].first);
    sort(all_distances_from_center.begin(), all_distances_from_center.end());
    
    // THIS COMBINES DISTANCES THAT DIFFER BY NUMERICAL PRECISION ERROR
    for(int c=1; c<particle_distances_from_center.size(); c++)
        if( abs(all_distances_from_center[c] - all_distances_from_center[c-1])<0.01)
            all_distances_from_center[c]=all_distances_from_center[c-1];
    
    all_distances_from_center.erase( unique( all_distances_from_center.begin(), all_distances_from_center.end() ), all_distances_from_center.end() );
    
    for(int c=0; c<all_distances_from_center.size() && c<10; c++)
        std::cout << "Distance " << c << " is " << all_distances_from_center[c] << '\n';
    std::cout << '\n';
    
    
    // WE CLASSIFY THE "NEIGHBORHOODNESS" OF EVERY PARTICLE IN THE SYSTEM
    // WE HAVE THREE KINDS:
    //   -1     NOT NEIGHBORS AT ALL
    //    0     CENTRAL PARTICLE
    //    1     FULL NEIGHBORS
    //    2     FRACTIONAL NEIGHBORS
    std::vector <int> neighborhoodness(number_of_particles,-1);
    
    std::vector<int>                  full_neighbors_of_central;
    std::vector<int>                  fractional_neighbors_of_central;
    std::vector<std::pair<int, int> > fractional_neighbor_pairs;

    // CENTRAL PARTICLE
    neighborhoodness[0]=0;

    // CREATES LIST OF FULL NEIGHBORS OF CENTRAL PARTICLE
    for(int c=0; c<neighbors_list_c[0]; c++)
    {
        full_neighbors_of_central.push_back(neighbors_list[0][c]);
        neighborhoodness[neighbors_list[0][c]]=1;
        fractional_neighbor_pairs.push_back(std::make_pair(0, neighbors_list[0][c]));

    }
    
    // ITERATES OVER ALL PARTICLES, IF THEY ARE AT CORRECT DISTANCES THEN THEY ARE FRACTIONAL NEIGHBORS
    // SHOULD EVENTUALLY BE UPDATED TO A COMBINATORIAL DEFINITION
    for(int c=0; c<number_of_particles; c++) if(neighborhoodness[c]==-1)
    {
        if( abs(particle_distances_from_center[c].first - all_distances_from_center[2]) < 0.01) // CAN BE UPDATED FOR DIFFERENT DISTANCES
        {
            fractional_neighbors_of_central.push_back(c);
            neighborhoodness[c]=2;
            fractional_neighbor_pairs.push_back(std::make_pair(0, c));
        }
    }
    
    
    // PRINTS LIST OF FULL AND FRACTIONAL NEIGHBORS
    std::cout << "List of full and fractional neighbors:\n";
    for(int c=0; c<number_of_particles; c++) if(neighborhoodness[c]!=-1)
        std::cout << c << '\t' << neighborhoodness[c] << '\t' << coordinates[0][c] << '\t' << coordinates[1][c] << '\t' << coordinates[2][c] << '\n';
    
    
    // ITERATES OVER ALL PAIRS OF FULL OR FRACTIONAL NEIGHBORS.  IF THEY ARE AT CORRECT DISTANCES
    // THEN THEY ARE POTENTIAL NEIGHBORS WHICH WE WILL NEED TO CHECK.
    for(int c=1; c<number_of_particles; c++) if(neighborhoodness[c]!=-1) //
        for(int d=c+1; d<number_of_particles; d++) if(neighborhoodness[d]!=-1)
        {
            double dx = coordinates[0][c] - coordinates[0][d];
            double dy = coordinates[1][c] - coordinates[1][d];
            double dz = coordinates[2][c] - coordinates[2][d];
            
            if(dx > supercell_edges[0][0]/2.) dx -= supercell_edges[0][0];
            if(dx <-supercell_edges[0][0]/2.) dx += supercell_edges[0][0];
            if(dy > supercell_edges[1][1]/2.) dy -= supercell_edges[1][1];
            if(dy <-supercell_edges[1][1]/2.) dy += supercell_edges[1][1];
            if(dz > supercell_edges[2][2]/2.) dz -= supercell_edges[2][2];
            if(dz <-supercell_edges[2][2]/2.) dz += supercell_edges[2][2];
            
            double distance = sqrt(dx*dx+dy*dy+dz*dz);
            
            //if(c==0 && distance==all_distances_from_center[2])
            //    fractional_neighbor_pairs.push_back(std::make_pair(c, d));
            
            //else
                if((c!=0) && ( abs(distance - all_distances_from_center[2]) < 0.01 || abs(distance - all_distances_from_center[2]) < 0.01))
            {
                //bool nbr = 0;
                //for(int e=0; e<neighbors_list_c[c]; e++)
                //    if(neighbors_list[c][e]==d) nbr=1;
                //if(nbr==0)
                fractional_neighbor_pairs.push_back(std::make_pair(c, d));
            }
        }
    
    std::cout << "We have " << full_neighbors_of_central.size() << " full neighbors of central particles\n";
    std::cout << "We have " << fractional_neighbors_of_central.size() << " fractional neighbors of central particles\n";
    std::cout << "We have " << fractional_neighbor_pairs.size() << " fractional neighbors among all neighbors\n";
    std::cout << "\n";
    std::cout << "List of stored neighbors to consider\n";
    for(int c=0; c<fractional_neighbor_pairs.size(); c++)
        std::cout << fractional_neighbor_pairs[c].first << "-" << fractional_neighbor_pairs[c].second << '\n';
    std::cout << '\n';
    std::cout << '\n';
    std::cout << '\n';
    ///exit(0);
    
    // OLD METHOD OF ADDING FRACTIONAL NEIGHBORS, BY CONSIDERING FULL NEIGHBORS OF FULL NEIGHBORS.
    // BUT I THINK THIS METHOD IS NOT GREAT.  UNTIL WE FIGURE OUT THE COMBINATORIAL APPROACH.
    /*
     for(int c=0; c<full_neighbors_of_central.size(); c++)
     {
     std::cout << "We are on neighbor " << c << " which has ID " << full_neighbors_of_central[c] << " and has "
     << neighbors_list_c[full_neighbors_of_central[c]] << " neighbors\n";
     
     for(int d=0; d<neighbors_list_c[full_neighbors_of_central[c]];d++)
     {
     int p2 = neighbors_list[full_neighbors_of_central[c]][d];
     
     std::cout << "About to check particle " << p2 << "\t";
     if(neighborhoodness[p2]==-1 && vec[p2].first == all_distances_from_center[2])
     {
     std::cout << "a match!\t" << vec[p2].first << '\t' << all_distances_from_center[2] << '\n';
     neighbors_of_full_neighbors_of_central.push_back(p2);
     neighborhoodness[p2]=2;
     }
     else if(neighborhoodness[p2]==-1)
     {
     std::cout << "mismatch!\t" << vec[p2].first << '\t' << all_distances_from_center[2] << '\n';
     }
     else
     std::cout << "Neither\n";
     }
     std::cout << '\n';
     }
     */
    
    // WE NEED TO WRITE TO CODE TO DETERMINE FIRST, SECOND, ETC NEIGHBORS
    // WE DO THIS BY FIRST LOOKING AT DISTANCES BETWEEN PARTICLE 0 AND ALL OTHER PARTICLES
    // THEN SORTING THAT, AND THEN USING THAT AS THE RIGHT INFO
    // LATER WE'LL MAKE SURE THIS IS DONE COMBINATORIALLY, USING
    // RELATIONSHIPS.  IDEALLY WE WOULD USE BETTER DEFINITIONS:
    //  PRIMARY: THOSE WHICH ARE NEIGHBORS VIA VORO++
    //  FRACTIONAL: THOSE WHICH SHARE SOME BOUNDARY ELEMENT IN COMMON
    
    // SO NOW WE HAVE A LIST OF ALL FULL AND FRACTIONAL NEIGHBORS OF 0
    // WE ALSO NEED TO FIGURE OUT AMONG THOSE PAIRS WHICH ONES ARE
    // FULL NEIGHBOR, FRACTIONAL, AND NON-NEIGHBORS.  HOW DO WE DO THAT?
    // AHA! SINCE EVERY PARTICLE IS IDENTICAL (IN THE LATTICE), THEN
    // WHICHEVER NUMBER GAVE US THE RIGHT DISTANCE FOR FULL/FRACTIONAL
    // IN THE FIRST ROUND IS THE SAME AS FOR THE SECOND ROUND.  LET'S
    // USE THAT (SO FAR WE DON'T KNOW HOW TO CHOOSE THIS AUTOMATICALLY).
    
    
    perturbation_size = 0.01;
    
    Filter filter;
    
    //Create my data
    //double initial_positions [9][3];
    //double    temp_positions [9][3];
    
    std::vector<double> initial_positions[3];
    initial_positions[0].resize(number_of_particles);
    initial_positions[1].resize(number_of_particles);
    initial_positions[2].resize(number_of_particles);
    
    std::vector<double> temp_positions[3];
    temp_positions[0].resize(number_of_particles);
    temp_positions[1].resize(number_of_particles);
    temp_positions[2].resize(number_of_particles);
    
    for(int c=0; c<number_of_particles; c++)
    {
        initial_positions[0][c] = coordinates[0][c];
        initial_positions[1][c] = coordinates[1][c];
        initial_positions[2][c] = coordinates[2][c];
    }

    
    // THESE ARE ALL POSSIBLE MOVEMENTS THAT WE WILL CONSIDER
    std::vector<generalized_motion> movements;
    movements.resize(fractional_neighbor_pairs.size());
    
    for(int c=0; c<fractional_neighbor_pairs.size(); c++)
    {
        movements[c].partA = fractional_neighbor_pairs[c].first;
        movements[c].partB = fractional_neighbor_pairs[c].second;
        
        movements[c].dx = initial_positions[0][movements[c].partB]-initial_positions[0][movements[c].partA];
        movements[c].dy = initial_positions[1][movements[c].partB]-initial_positions[1][movements[c].partA];
        movements[c].dz = initial_positions[2][movements[c].partB]-initial_positions[2][movements[c].partA];
        
        if(movements[c].dx > supercell_edges[0][0]/2.) movements[c].dx -= supercell_edges[0][0];
        if(movements[c].dx <-supercell_edges[0][0]/2.) movements[c].dx += supercell_edges[0][0];
        if(movements[c].dy > supercell_edges[1][1]/2.) movements[c].dy -= supercell_edges[1][1];
        if(movements[c].dy <-supercell_edges[1][1]/2.) movements[c].dy += supercell_edges[1][1];
        if(movements[c].dz > supercell_edges[2][2]/2.) movements[c].dz -= supercell_edges[2][2];
        if(movements[c].dz <-supercell_edges[2][2]/2.) movements[c].dz += supercell_edges[2][2];
        
//        std::cout << movements[c].dx << '\t' << movements[c].dy << '\t' << movements[c].dz <<'\n';
    }
    
    
    
    long unsigned int total_number = pow(2,fractional_neighbor_pairs.size());
    std::cout << "Want to test " << total_number << " options\n";
    for(int c=200000; c<total_number && c<1200000; c++)   // TO RUN IN SHORTER TIME
    {
        // THIS SAVES 0 AND 1'S FOR DETERMINE WHICH PAIRS OF
        // NEIGHBORS SHOULD BE BROUGHT CLOSER
        int moves[fractional_neighbor_pairs.size()];
        
        
        //        for(int d=0; d<fractional_neighbor_pairs.size();d++)
        //            moves[d] = rand() % 2;
        
        //moves[0]=1;
        for(int d=0; d<fractional_neighbor_pairs.size();d++)
            moves[d] = int(c/pow(2,d))%2;
        

        
        
        for(int d=0; d<number_of_particles; d++)
        {
            temp_positions[0][d] = initial_positions[0][d];
            temp_positions[1][d] = initial_positions[1][d];
            temp_positions[2][d] = initial_positions[2][d];
        }
        
        // HERE WE ITERATE OVER ALL OPTIONAL MOTIONS, AND ADD THEM TO THE POSITIONS
        for(int d=0; d<fractional_neighbor_pairs.size(); d++)
        {
            if(moves[d]==1)
            {
                //                if(c==32) std::cout << movements[d].partA << '\t' << movements[d].partB << '\t' <<
                //                    perturbation_size*movements[d].dx << '\t' << perturbation_size*movements[d].dy
                //                    << '\t' << perturbation_size*movements[d].dz << '\n' << '\n';
                
                if(movements[d].partA==0)
                {
                    temp_positions[0][movements[d].partB] -= perturbation_size*movements[d].dx;
                    temp_positions[1][movements[d].partB] -= perturbation_size*movements[d].dy;
                    temp_positions[2][movements[d].partB] -= perturbation_size*movements[d].dz;
                }
                else
                {
                    temp_positions[0][movements[d].partA] += perturbation_size*movements[d].dx;
                    temp_positions[1][movements[d].partA] += perturbation_size*movements[d].dy;
                    temp_positions[2][movements[d].partA] += perturbation_size*movements[d].dz;
                    
                    temp_positions[0][movements[d].partB] -= perturbation_size*movements[d].dx;
                    temp_positions[1][movements[d].partB] -= perturbation_size*movements[d].dy;
                    temp_positions[2][movements[d].partB] -= perturbation_size*movements[d].dz;
                }
            }
        }
        
        /*
        if(c==32)
        {
            for (int counter = 1; counter < number_of_particles; counter++)
                if(counter==1||
                   counter==2||
                   counter==3||
                   counter==4||
                   counter==13||
                   counter==14||
                   counter==49||
                   counter==51||
                   counter==61||
                   counter==194||
                   counter==195||
                   counter==206||
                   counter==243)
                {
                    std::cout << counter << '\t' << temp_positions[0][counter] << '\t' << temp_positions[1][counter]
                    << '\t' << temp_positions[2][counter] << '\n';
                }
            
        }
         */
        
        // BUILDS CONTAINER AND ADDS PARTICLES
        voro::particle_order vo;
        voro::container_periodic con (supercell_edges[0][0],0.,supercell_edges[1][1],0.,0.,supercell_edges[2][2],1,1,1,1);
        
        double x, y, z;
        x = initial_positions[0][0];
        y = initial_positions[1][0];
        z = initial_positions[2][0];
        con.put(vo,0,x,y,z);

        for (int counter = 1; counter < number_of_particles; counter++)
        {
            x = temp_positions[0][counter];//+distribution(generator);
            y = temp_positions[1][counter];//+distribution(generator);
            z = temp_positions[2][counter];//+distribution(generator);
            if(neighborhoodness[counter]!=-1)
                con.put(vo,counter, x,y,z);
        }
        
        
        
        
        
        voro::c_loop_order_periodic vlo(con,vo);
        voro::voronoicell_neighbor vcell;
        
        // WE COMPUTE THE VORONOI TOPOLOGY OF THE CENTRAL PARTICLE
        if(vlo.start())
        {
            
            std::vector<int> testing;
            
            
            if(con.compute_cell(vcell,vlo))
            {
                vcell.neighbors(testing);
                all_wvectors[0] = calc_wvector(vcell,0);
                neighbors_list_c[0] = neighbors_list[0].size();
            }
            
            /*
            if(testing.size()==12)
            {
                std::cout << c << '\n';
                for(int d=0; d<fractional_neighbor_pairs.size();d++)
                    std::cout << moves[d] << ",";
                std::cout << '\n';
                
                //exit(0);
            }
            if(c==32)
            {
                std::cout << testing.size() << '\t';
                sort(testing.begin(), testing.end());
                for(int k=0; k<testing.size(); k++)
                    std::cout << testing[k] << ",";
                std::cout << '\n';
                
                //std::cout << "leaving\n\n\n";
                //exit(0);
                
                int  length = all_wvectors[0].back();                          // LENGTH OF WVECTOR
                int plength = all_wvectors[0].end()[-2];//back()-1;            // LENGTH OF PVECTOR
                
                std::cout << c << '\t';
                
                std::cout << all_wvectors[0][length+plength-3] << '\t';     // NUMBER OF FACES
                
                std::cout << '(';                                           // P VECTOR
                for(int d=length; d<length+plength-4; d++)
                    std::cout << all_wvectors[0][d] << ',';
                std::cout << all_wvectors[0][length+plength-4] << ')' << '\t';
                
                std::cout << '(';                                            // WEINBERG VECTOR
                for(int d=0; d<length; d++)
                    std::cout << all_wvectors[0][d] << ',';
                std::cout << 1 << ')' << '\t';
                
                std::cout << all_wvectors[0][length+plength-2] << '\t';     // SYMMETRIES
                std::cout << all_wvectors[0][length+plength-1] << '\t';     // CHIRALITY
                std::cout << all_wvectors[0][length+plength]   << '\t';     // STABLE
                
                std::cout << '\n';
                exit(0);
            }
             */
        }
        
        // ADDS TOPOLOGY OF FIRST VORONOI CELL, THE ONE IN THE MIDDLE
        filter.increment_or_add(all_wvectors[0],1);
    }
    
    std::stringstream my_filename;
    my_filename << "testing.wvectors";
    filename_output = my_filename.str();
    filter.print_distribution(filename_output);
    
    std::cout << "We have " << filter.get_size() << " types\n";
    
    return 0;
}









